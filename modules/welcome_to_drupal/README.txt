CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------
The Welcome to Drupal module displays a form where a visitor enters his/her 
name.
The module also provides a view which shows the visitors statistics.
 
 * For a full description of the module, visit the project page:
   http://drupal.org/sandbox/papoyanani/2643018.


REQUIREMENTS
------------

This module requires the following modules:

 * Views (https://drupal.org/project/views)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.
