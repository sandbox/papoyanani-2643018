<?php

/**
 * @file
 * Welcome to Drupal.
 */

/**
 * Implements hook_views_data().
 */
function welcome_to_drupal_views_data() {
  $data = array();

  $data['visitors']['table']['group'] = t('Visitors');

  $data['visitors']['table']['base'] = array(
    'title' => t('Visitors'),
    'help' => t('Visitors List'),
  );
  $data['visitors']['vid'] = array(
    'title' => t('ID'),
    'help' => t('The record ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  // The FName field.
  $data['visitors']['fname'] = array(
    'title' => t('First Name'),
    'help' => t('First name.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  // The Last name field.
  $data['visitors']['lname'] = array(
    'title' => t('Last Name'),
    'help' => t('Last name.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['visitors']['visits'] = array(
    'title' => t('Counter Value'),
    'help' => t('Counter Value.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  return $data;
}
